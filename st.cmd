#!/usr/bin/env iocsh.bash
# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require essioc
require huginn
 
# -----------------------------------------------------------------------------
# Utgard-Lab
# -----------------------------------------------------------------------------
epicsEnvSet("LOCATION",             "Utgard")
epicsEnvSet("SYS",                  "SE-SEE:")
epicsEnvSet("DEV",                  "SE-HUGINN-001")
epicsEnvSet("PREFIX",               "$(SYS)$(DEV)")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(huginn_DIR)db/")
# -----------------------------------------------------------------------------
# cryostat sub (sample)
epicsEnvSet("CRYO_SUB",  "SE-SEE:SE-LS336-003")
epicsEnvSet("T_BASE",               "KRDG3")
epicsEnvSet("T_PELTIER_TOP",        "KRDG0")
epicsEnvSet("T_PELTIER_BOTTOM",     "KRDG1")
epicsEnvSet("T_SAMPLE",             "KRDG2")
# -----------------------------------------------------------------------------
# cryostat main
epicsEnvSet("CRYO_MAIN",  "SE-SEE:SE-LS336-002")
epicsEnvSet("T_MAIN_BASE",          "KRDG1")
epicsEnvSet("T_MAIN_HEATER",        "KRDG0")
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Loading databases
# -----------------------------------------------------------------------------
# Huginn
iocshLoad("$(huginn_DIR)huginn.iocsh")

# E3 Common databases
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# -----------------------------------------------------------------------------
# Huginn and LakeShores have some default parameters loaded when the IOC starts
# -----------------------------------------------------------------------------
# Calling a script which copies default settings .sav to specific AS_TOP
# input params:
#     $1 AUTOSAVE_DIR
#     $2 AUTOSAVE_FILE
# -----------------------------------------------------------------------------
epicsEnvSet("AUTOSAVE_DIR",   "$(AS_TOP)/$(IOCNAME)/save")
epicsEnvSet("AUTOSAVE_FILE",  "$(huginn_DIR)defaultHuginnSettings.sav")
system "$(huginn_DIR)huginnCopyAutosaveSettings.sh $(AUTOSAVE_DIR) $(AUTOSAVE_FILE)"


# -----------------------------------------------------------------------------
# Huginn settings being restored
# -----------------------------------------------------------------------------
# this line will restore data after initialization of array, during pass0 memory for the array is not yet allocated
set_pass1_restoreFile("$(AUTOSAVE_FILE)", "P=$(PREFIX), CRYO_SUB=$(CRYO_SUB), CRYO_MAIN=$(CRYO_MAIN)")
# Do not create datedBeckupfiles every time when ioc stops. It will create only a backaup file of myArrays.sav named .sav.bu
save_restoreSet_DatedBackupFiles(0)

# -----------------------------------------------------------------------------
# Starting sequencer program
# -----------------------------------------------------------------------------
seq snl_huginn("P=$(PREFIX), CRYO_SUB=$(CRYO_SUB), T_BASE=$(T_BASE)")

# -----------------------------------------------------------------------------
# Starting the IOC
# -----------------------------------------------------------------------------
iocInit()
